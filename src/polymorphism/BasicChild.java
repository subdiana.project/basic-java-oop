package polymorphism;

public class BasicChild extends Basic{
    BasicChild(String name) {
        super(name);
    }

    void hallo(String param) {
        System.out.println("Hallo : " + param + " My name is child " + super.name);
    }
}
