package polymorphism;

public class BasicImpl {
    public static void main(String[] args) {
        var basic = new Basic("iyan");
        basic.hallo("hamizan");
        helloImpl( new Basic("iyan-impl"));
        CheckNCast(basic);
        System.out.println();
        System.out.println("==================================");
        System.out.println();
        basic = new BasicChild("khawarizmi");
        basic.hallo("iyan");
        helloImpl( new BasicChild("khawarizmi-impl"));
        CheckNCast(basic);
    }

    static void helloImpl(Basic basic) {
        System.out.println("hallo impl : " + basic.name);
    }

    static void CheckNCast(Basic basic) {
        if (basic instanceof BasicChild) {
            BasicChild basicChild = (BasicChild) basic;
            basicChild.hallo("iyan-BasicChild");
        } else {
            basic.hallo("iyan-Basic");

        }
    }
}
