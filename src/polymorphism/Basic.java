package polymorphism;

public class Basic {
    String name;
    Basic(String name){
        this.name = name;
    }

    void hallo(String param){
        System.out.println("Hallo : " + param + " My name is main " + this.name);
    }
}
