package interfaces;

public interface BasicInterface {
    void voice();
    int getAge(int age);

    default boolean isAlive() {
        return true;
    }
}