package interfaces;

public class BasicImpl implements BasicInterface, InheritInterface {
    @Override
    public void voice() {
        System.out.println("Voice Hallo");
    }

    @Override
    public int getAge(int age) {
        return age;
    }

    @Override
    public String getAddress() {
        return "Rangkasbitung";
    }
}
