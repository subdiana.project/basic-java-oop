package interfaces;

public class BasicMain {
    public static void main(String[] args) {
        var basic = new BasicImpl();
        basic.voice();
        System.out.println("Ages : " + basic.getAge(20));
        System.out.println("address : " + basic.getAddress());
        System.out.println("default methods : " + basic.isAlive());
    }
}
