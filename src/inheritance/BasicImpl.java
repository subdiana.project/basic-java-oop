package inheritance;

public class BasicImpl{
    public static void main(String[] args) {
       var basic = new Basic();
       basic.printInheritFunc("main");

       var basicChild = new BasicChild();
       basicChild.printInheritFunc("child");
    }
}
