package inheritance;

public class OverridingImpl{
    public static void main(String[] args) {
        var overrider = new Override();
        overrider.printOverrideFunc("main");

        var overriderChild = new OverrideChild();
        overriderChild.printOverrideFunc("overriderChild");
        overriderChild.printParentOverrideFunc();
    }

}
