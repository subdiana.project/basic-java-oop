package inheritance;

public class OverrideChild extends Override{

    static void printOverrideFunc(String param) {
        System.out.println("Override function from : " + param);
    }

    void printParentOverrideFunc() {
        super.printOverrideFunc("overriderChild");
    }
}
