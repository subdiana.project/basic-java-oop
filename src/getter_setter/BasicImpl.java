package getter_setter;

public class BasicImpl {
    public static void main(String[] args) {
        var basic = new Basic();

        basic.setName("iyan");
        System.out.println("getter name : " + basic.getName());
        System.out.println();
        basic.setCheck(true);
        System.out.println("getter check : " + basic.isCheck());
    }
}
