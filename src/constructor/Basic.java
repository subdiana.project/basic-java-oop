package constructor;

class Basic {
    String name;
    String age;

    Basic(String name, String age) {
        this.name = name;
        this.age = age;
    }

    void sayHello() {
        System.out.println("hallo : " + this.name + " || age : " + this.age);
    }
}
