package constructor;

class OverloadingObject {
    public static void main(String[] args) {
        var constructor = new Overloading("hamizan", "6");
        System.out.println("with main constructor");
        constructor.sayHello();

        var overloading = new Overloading("khawarizmi");
        System.out.println("with overloading constructor");
        overloading.sayHello();
    }
}
