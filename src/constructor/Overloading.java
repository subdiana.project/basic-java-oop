package constructor;

class Overloading {
    String name;
    String age;

    Overloading(String name, String age) {
        this.name = name;
        this.age = age;
    }

    Overloading(String name) {
        this.name = name;
    }

    void sayHello() {
        System.out.println("hallo : " + this.name + " || age : " + this.age);
    }
}
