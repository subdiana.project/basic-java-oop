package constructor;

class BasicObject {
    public static void main(String[] args) {
        var basicClass = new Basic("hamizan", "6");
        System.out.println(basicClass);
        System.out.println("name : " + basicClass.name + " || age : " + basicClass.age);

        basicClass.sayHello();
    }
}
