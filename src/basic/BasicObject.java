package basic;

class BasicObject {
    public static void main(String[] args) {
        var basicClass = new Basic();
        System.out.println(basicClass);
        /*
            assign attribute
         */

        basicClass.name = "iyan";
        basicClass.age = "32";

        System.out.println("name : " + basicClass.name + " || age : " + basicClass.age);

        basicClass.sayHello();
    }
}
